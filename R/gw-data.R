#' Landsat time series of a vegetation index for pixels associated with a mule
#' deer study on the WY-CO-UT border.
#'
#' A dataset containing time series of Enhanced Vegetation Index (EVI) for 25
#' pixels selected at random from the approximate study area of Aikens et al.
#' (2017; https://www.sciencebase.gov/catalog/item/58b9a169e4b0c1723b8a0395/).
#'
#' @docType data
#' @format A data frame with 18,591 rows and 4 variables:
#' \describe{
#'   \item{date}{acquisition date (YYYY-MM-DD)}
#'   \item{evi}{Enhanced Vegetation Index}
#'   \item{series}{the Landsat satellite/mission from which the EVI observation
#'   was taken}
#'   \item{loc_id}{a unique identifier for the pixel from which the time series
#'   comes}
#'   ...
#' }
"mule_deer_evi"

#' Ancillary spatial data for individual locations associated with the WY-CO-UT
#' mule deer study.
#'
#' A dataset containing ancillary spatial information for the
#'
#' @format A data frame with 25 rows and 6 variables:
#' \describe{
#'   \item{loc_id}{a unique identifier for the pixel (can be used to join this
#'   data to records in \code{mule_deer_evi})}
#'   \item{elev}{elevation, in meters}
#'   \item{chili}{continuous heat-insolation load index}
#'   \item{nlcd_type}{National Land Cover Database (NLCD) land cover type}
#'   \item{lat}{latitude}
#'   \item{lon}{longitude}
#'   ...
#' }
"mule_deer_anc_space"

#' 3-day greenness data from the PhenoCam at Harvard Forest.
#' For more info: https://phenocam.sr.unh.edu/webcam/sites/harvard/
#'
#' @docType data
#' @format a data.table with 1344 rows and 4 columns
#' \describe{
#'   \item{date}{date (YYYY-MM-DD)}
#'   \item{gcc}{Greenness Chromatic Coordinate (GCC)}
#' }
"harvard_phenocam"
