#' greenwave: A package for fitting "green wave" models to time series of 
#' vegetation indices.
#'
#' The greenwave package provides several categories of functions: TBD.
#'
#' @section Package functions:
#' The greenwave functions ...
#'
#' @docType package
#' @name greenwave
NULL
