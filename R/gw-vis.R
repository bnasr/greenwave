#' Visualization
#'
#' Visualization functions for greenwave model objects.
#'
#' TBD....
#'
#' @param param_key a parameter key obtained using \code{gw_param_key}
#' @param timing,greenness the vectors of timing and greenness values to be
#' joined to the parameter key
#' @param index parameter index (integer)
#' @name gw-vis
NULL
#' @import ggplot2
#' @importFrom ggthemes theme_hc
#' @importFrom ggrepel geom_label_repel
#' @importFrom magrittr %>%
#' @importFrom dplyr select rowwise mutate ungroup
#' @importFrom tidyr spread
#' @keywords visualization, plotting
#' @examples
#' # Get plot.
#' t <- c("t[0]" = 0, "t[1]" = 2, "t[2]" = 5)
#' g <- c("g[0]" = 1, "g[1]" = 1, "g[2]" = 3)
#' gw_vis_lin_fun(gw_param_key(), t, g)
#' @rdname gw-vis
gw_coord_label <- function(index) {
  paste0(
    "list(",
    paste(paste0(c("t", "g"), "[", index, "]"), collapse = ", "),
    ")"
  )
}

#' @rdname gw-vis
#' @export
gw_vis_lin_fun <- function(param_key, timing, greenness) {
  data <- gw_join_param_vals(param_key, timing, greenness) %>%
    select(vector, value, transition, index) %>%
    spread(vector, value) %>%
    rowwise() %>%
    mutate(label = gw_coord_label(index)) %>%
    ungroup()
  ggplot() +
    geom_line(data = data, aes(x = t, y = g), size = 1.25, color = "white") +
    geom_label_repel(
      data = data, aes(x = t, y = g, label = label),
      parse = TRUE
    ) +
    theme_hc(style = "darkunica", base_size = 13) +
    labs(x = "Timing", y = "Greenness")
}
