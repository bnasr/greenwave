FROM cspinc/r-packaging:latest

RUN install2.r --error \
    ggplot2 \
    dplyr \
    tidyselect \
    tidyr \
    tibble \
    magrittr \
    ggthemes \
    ggrepel \
    ggforce \
    mvtnorm

# Stan (and rstan) and cleanup
RUN mkdir -p /root/.R
COPY Makevars /root/.R/Makevars
RUN export MAKEFLAGS="-j4"
RUN R -e "install.packages('rstan', type = 'source')"
RUN apt-get autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt /var/cache/apt
RUN rm -rf /tmp/downloaded_packages/ /tmp/*.rds

# JAGS (and rjags) and cleanup
RUN wget http://ftp.debian.org/debian/pool/main/j/jags/jags_4.3.0.orig.tar.gz \
    -P /usr/local && \
    tar xvzf /usr/local/jags_4.3.0.orig.tar.gz -C /usr/local/bin && \
    rm /usr/local/jags_4.3.0.orig.tar.gz && \
    cd /usr/local/bin/JAGS-4.3.0 && \
    ./configure && \
    make && \
    make install && \
    /sbin/ldconfig && \
    R -e "install.packages('rjags', \
        configure.args='--with-jags-include=/usr/local/include/JAGS \
            --with-jags-lib=/usr/local/lib/JAGS \
            --with-jags-modules=/usr/local/lib/JAGS/modules')" \
    R -e "install.packages('R2jags')"
