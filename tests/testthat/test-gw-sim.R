context("test-gw-sim")

test_that("the expected value of observations and the mean of observations is 
          identical (in space and time) under zero-variance conditions", {
  
  obs_error <- 0
  re_variance <- c('1' = 0, '2' = 0, '3' = 0, '4' = 0)
  
  re_space <- list(
    mu = c(
      mu_t = c('1' = 100, '2' = 150, '3' = 225, '4' = 300),
      mu_g = c('1' = .1, '2' = .6, '3' = .5, '4' = .15)
    ),
    sd = c(
      sd_t = re_variance,
      sd_g = re_variance
    ),
    rho = NULL
  )
  re_time <- list(
    mu = c(
      mu_t = c('1' = 0, '2' = 0, '3' = 0, '4' = 0),
      mu_g = c('1' = 0, '2' = 0, '3' = 0, '4' = 0)
    ),
    sd = c(
      sd_t = re_variance,
      sd_g = re_variance
    ),
    rho = NULL
  )
  
  scaffold <- crossing(loc_id = letters[1:5], 
                       year = c(1990, 1991), 
                       doy = sample(1:365, 5)) %>% 
    mutate(date = lubridate::parse_date_time(paste(year, doy, sep = ' '), 
                                             'Y!, j'))
  
  d_sim <- gw_sim_spacetime(
    data = scaffold,
    mu_j = re_space$mu, sd_j = re_space$sd, Omega_j = gw_cor_mat(re_space),
    mu_k = re_time$mu, sd_k = re_time$sd, Omega_k = gw_cor_mat(re_time),
    sd_y = obs_error,
    complete = TRUE)
  
  
  y_equals_y_hat <- function(y, y_hat) {
    y == y_hat
  }
  
  pixels_meet_expectations <- d_sim %>% 
    spread(j, y, sep = '_') %>% 
    mutate_at(vars(matches('j_[[:digit:]]')), y_equals_y_hat, y_hat = .$y_hat) %>% 
    select(matches('j_[[:digit:]]')) %>% 
    distinct
  
  expect_true(all(unlist(pixels_meet_expectations)))
  
})
